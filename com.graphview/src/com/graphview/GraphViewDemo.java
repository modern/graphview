package com.graphview;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.graphview.GraphView.GraphViewData;
import com.graphview.GraphView.GraphViewSeries;

public class GraphViewDemo extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		LineGraphView graphView = new LineGraphView(
				this
				, ""
		);
		graphView.addSeries(new GraphViewSeries(new GraphViewData[] {
				new GraphViewData(0, 2.0d)
				, new GraphViewData(2, 2.0d)
				, new GraphViewData(3, 2.5d)
		}));
		
		setContentView(graphView);
	}
}
